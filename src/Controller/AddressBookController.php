<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactType;
use App\Repository\ContactRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AddressBookController extends \Symfony\Bundle\FrameworkBundle\Controller\AbstractController
{
    /**
     * @Route("/", name="address_book_index")
     *
     * @param ContactRepository $contactRepository
     *
     * @return Response
     */
    public function index(ContactRepository $contactRepository)
    {
        // get all contacts
        $contacts = $contactRepository->findAll();

        return $this->render('index.html.twig', [
            'contacts' => $contacts,
        ]);
    }

    /**
     * @Route("/new", name="address_book_create")
     *
     * @param Request $request
     * @param EntityManagerInterface $em
     *
     * @return RedirectResponse|Response
     */
    public function create(Request $request, EntityManagerInterface $em)
    {
        // create form
        $form = $this->createForm(ContactType::class);

        // handle request
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // process creating contact
            $contact = $form->getData();
            $contact->setUniqueId(uniqid('', true));
            $em->persist($contact);
            $em->flush();
            $this->addFlash('success', 'Kontakt byl přidán');

            return $this->redirectToRoute('address_book_index');
        }

        return $this->render('new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{firstname}-{surname}-{id}", name="address_book_edit")
     *
     * @param Request $request
     * @param string $firstname
     * @param string $surname
     * @param int $id
     * @param ContactRepository $contactRepository
     * @param EntityManagerInterface $em
     *
     * @return RedirectResponse|Response
     */
    public function edit(Request $request, string $firstname, string $surname, int $id, ContactRepository $contactRepository, EntityManagerInterface $em)
    {
        // find contact
        $contact = $contactRepository->findOneBy(['firstname' => $firstname, 'surname' => $surname, 'id' => $id]);

        // check if it exists
        if ( ! $contact instanceof Contact) {
            $this->addFlash('info', 'Kontakt neexistuje');

            return $this->redirectToRoute('address_book_index');
        }

        // create form
        $form = $this->createForm(ContactType::class, $contact);

        // handle request
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // process editing contact
            $form->getData();
            $em->flush();
            $this->addFlash('success', 'Kontakt byl upraven');

            return $this->redirectToRoute('address_book_index');
        }

        return $this->render('edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{uniqueId}/remove", name="address_book_remove")
     *
     * @param Contact $contact
     * @param EntityManagerInterface $em
     *
     * @return RedirectResponse
     */
    public function remove(Contact $contact, EntityManagerInterface $em)
    {
        // remove entity
        $em->remove($contact);
        $em->flush();

        $this->addFlash('success', 'Kontakt byl smazán');

        return $this->redirectToRoute('address_book_index');
    }
}
