## Address book
Symfony based address book

# Installation
- clone project from git
- install dependencies (run composer install)
- copy .env to .env.local and configure database connection
- create database (php bin/console doctrine:database:create)
- run migrations (php bin/console doctrine:migrations:migrate)
- run application, either using Symfony server (symfony server:start) or setting up a virtual local host